import Home from '../pages/home.js';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import LazyRoute from 'lazy-route';

const Main = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route
        path="/profile"
        render={props => <LazyRoute {...props} component={import('../pages/profile.js')} />}
      />
    </Switch>
  </Router>
);

ReactDom.render(<Main />, document.getElementById('app'), async () => {
  if (process.env.NODE_ENV != 'development') {
    await import('./pwa.js');
    window.performance.mark('mark_fully_loaded');
  }
});
